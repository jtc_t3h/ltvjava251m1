package chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmHelloWorld extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmHelloWorld frame = new FrmHelloWorld();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmHelloWorld() {
		setTitle("Hello World");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 164);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Nhập họ tên");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(44, 12, 100, 16);
		contentPane.add(lblNewLabel);

		txtHoTen = new JTextField();
		txtHoTen.setBounds(156, 6, 288, 29);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);

		JLabel lblHienThi = new JLabel("");
		lblHienThi.setBounds(5, 40, 439, 29);
		contentPane.add(lblHienThi);

		JButton btnNewButton = new JButton("Xuất lời chào");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Xử lý nút "Xuất lời chào" khi người dùng click button
				// B1: Lấy thông tin họ tên người dùng nhập -> lấy giá trị từ thuộc text
				String hoTen = txtHoTen.getText();
				// B2. Xuất lời chào -> Thiết lập giá trị cho lblHienThi -> sử dụng thuộc tính
				// text
				lblHienThi.setText("Chào bạn " + hoTen + ", chào mừng bạn đến với ngôn ngữ lập trình Java.");
			}
		});
		btnNewButton.setBounds(157, 81, 127, 29);
		contentPane.add(btnNewButton);
	}

}
