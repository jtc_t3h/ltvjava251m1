package chapter1;

public class TestArgs {

	public static void main(String[] args) {
		System.out.println("Length = " + args.length);
		
		// Truy xuat phan tu thu nhat -> idx = 0;
		System.out.println("args[0] = " + args[0]);
		
		// Duyệt các phần tử của mảng -> sử dụng cấu trúc for-each
		for (String element: args) {
			System.out.println(element);
		}
	}

}
