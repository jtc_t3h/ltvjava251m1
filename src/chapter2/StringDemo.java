package chapter2;

import java.util.StringTokenizer;

public class StringDemo {

	public static void main(String[] args) {

		String s1 = "hello";
		String s2 = new String("hello");
		String s3 = "hello";
		
		String s4 = "  ";
		System.out.println(s4.isEmpty()); // NullPointerException
		
		System.out.println(s1 == s2);
		System.out.println(s1.equals(s2));
		System.out.println(s1 == s3);
		
		
		s1.concat(" world");
		System.out.println(s1);
		
		StringBuilder sb = new StringBuilder();
		sb.append("hello world");
		sb.reverse();
		System.out.println(sb);
		
		String s = "java programming basic";
		StringTokenizer st = new StringTokenizer(s);
		System.out.println(st.countTokens());  // 3
		
		// Duyệt danh sách các Token
//		int countToken = st.countTokens();
//		for (int idx = 0; idx < countToken; idx++) {
//			System.out.println(st.nextToken());
//		}
		while (st.hasMoreTokens()) {
			System.out.println(st.nextToken());
		}
		
		// File CSV = comma seperate value
		String csv = "1, Nguyễn Văn A, 20/10/2014";
		StringTokenizer st2 = new StringTokenizer(csv, ",");
		while (st2.hasMoreTokens()) {
			System.out.println(st2.nextElement().toString().trim());
		}
	}

}
