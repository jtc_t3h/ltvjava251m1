package chapter7;

public class GenericDemo {
	
	// Generic class
	private static class Inner<E>{
		private E e;
		
		public E getE() {
			return e;
		}

		public void setE(E e) {
			this.e = e;
		}

		public void printValue() {
			System.out.println(e);
		}
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Integer [] iArr = {1,2,3,4,5};
		Double [] dArr = {1.1, 2.2, 3.3};
		String [] sArr = {"Hello",  "world", "java"};
		
//		for (int e: iArr) {
//			System.out.println(e);
//		}
//		
//		for (double e: dArr) {
//			System.out.println(e);
//		}
//		
//		for (String e: sArr) {
//			System.out.println(e);
//		}
		
		GenericDemo g = new GenericDemo();
		
		g.printArray(iArr); // T la Integer
		g.printArray(dArr);
		g.printArray(sArr);
		
		Inner<String> inS = new Inner<String>();
		inS.setE("Hello");
		
	}

	// Generic method --> khai bao <T> thi compile hieu la khai bao generic
	public <T> void printArray(T [] arr) {
		for (T e: arr) {
			System.out.println(e);
		}
	}
}
