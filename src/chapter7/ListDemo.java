package chapter7;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Tạo đối tượng List
		List list = new LinkedList();
		
		// Thêm phần tử vào List
		list.add(123);
		list.add("hello");
		list.add(1.1);
		
		//duyệt các phần tử
		for (Object e: list) {
			System.out.println(e);
		}
	}

}
