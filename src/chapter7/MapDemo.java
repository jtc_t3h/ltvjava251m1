package chapter7;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Map<String, Integer> map = new HashMap<String, Integer>();
		
		map.put("123", 123);
		map.put("abc", 456);
		map.put("123", 789);
		
		for (String k: map.keySet()) {
			System.out.println(k + " = " + map.get(k));
		}
	}

}
