package chapter7;

import java.util.HashSet;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {
		Set<Integer> set = new HashSet<Integer>();
		
		set.add(456);
		set.add(123);
		set.add(456);
		
		for (int e: set) {
			System.out.println(e);
		}
	}
}
