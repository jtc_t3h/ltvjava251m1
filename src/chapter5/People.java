package chapter5;

public class People {

	// instance variable
	private String name;
	private int birth;

	// Constructor method
	public People() {
		name = null;
		birth = 0;
	}

	public People(String name, int birth) {
		this.name = name;
		this.birth = birth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBirth() {
		return birth;
	}

	public void setBirth(int birth) {
		this.birth = birth;
	}
	
	@Override
	public String toString() {
		return name + " " + birth;
	}
}




