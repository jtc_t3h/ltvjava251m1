package chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Comparator;
import java.awt.event.ActionEvent;

public class FrmSort extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtBirth;
	
	/**
	 * Mang chua danh sach people
	 */
	private People[] peoples = new People[100];
	private int idxCurrent = 0;
	private JList list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmSort frame = new FrmSort();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmSort() {
		setTitle("Sort by Age");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 427, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(6, 20, 61, 16);
		contentPane.add(lblName);
		
		JLabel lblBirth = new JLabel("Birth");
		lblBirth.setBounds(6, 47, 61, 16);
		contentPane.add(lblBirth);
		
		txtName = new JTextField();
		txtName.setBounds(83, 15, 130, 26);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		txtBirth = new JTextField();
		txtBirth.setColumns(10);
		txtBirth.setBounds(83, 42, 130, 26);
		contentPane.add(txtBirth);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Code here
				// B1: Lấy dữ liệu người nhập về -> tạo ra đối tượng People -> thêm vào danh sách peoples
				String name = txtName.getText().trim();
				int birth = Integer.parseInt(txtBirth.getText().trim());
				People p = new People(name, birth);
				peoples[idxCurrent++] = p;
//				idxCurrent = idxCurrent + 1;
				
				// B2. Sắp xếp danh sách people theo age
				People subPeople[] = sortByAge();
				
				// B3. Nạp dữ liệu lên cho JList
				DefaultComboBoxModel<People> model = (DefaultComboBoxModel<People>) list.getModel();
				model.removeAllElements();
				for (People element: subPeople) {
					model.addElement(element);
				}
				list.setModel(model);
				
			}
		});
		btnAdd.setBounds(302, 15, 117, 51);
		contentPane.add(btnAdd);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 85, 412, 173);
		contentPane.add(scrollPane);
		
		list = new JList();
		scrollPane.setViewportView(list);
		// Khởi tạo dữ liệu cho List
		list.setModel(new DefaultComboBoxModel<People>());
	}
	
	private People[] sortByAge() {
		// B1. Tạo ra mảng con chỉ gồm những phần tử có giá trị (loại phần tử null)
		People subPeople[] = new People[idxCurrent];
		for (int idx = 0; idx < idxCurrent; idx++) {
			subPeople[idx] = peoples[idx];
		}
		
		// B2. Sort by age
		Comparator<People> com = new Comparator<People>() {

			@Override
			public int compare(People o1, People o2) {
				// TODO Auto-generated method stub
				return o1.getBirth() - o2.getBirth();
			}
			
		};
		Arrays.sort(subPeople, com);
		return subPeople;
	}
}
