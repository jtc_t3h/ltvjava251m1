package chapter5;

public class TestClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int var = 1;
		int[] arr = {1};
		
		System.out.println(var); // 1
		System.out.println(arr[0]); // 1
		
		TestClient t = new TestClient();
		t.changeValue(var);
		changeValue(arr);
		
		System.out.println(var); // 1? - by pass value | 11? - by pass reference => 1
		System.out.println(arr[0]); // 1? 11? => 11
		
		varArgMethod();
		varArgMethod(1,2,3);
	}
	
	public void changeValue(int var) {
		var += 10;
	}

	public static void changeValue(int [] arr) {
		arr[0] += 10;
	}
	
	public static void varArgMethod(int ... args) {
		// duyệt danh sách đối số
		for (int i: args) {
			System.out.println(i);
		}
	}
}
