package chapter4;

import java.util.Arrays;
import java.util.Comparator;

public class ArrayDemo {

	public static void main(String[] args) {
		
		// 1. Mảng 1 chiều
		// Khai báo mảng -> 3 cách
		int [] arrInt = new int[5];
		String arrStr[] = {"hello", "world", "java"};
		Double arrDou[] = new Double[] {1.1, 2.2, 3.3};
		
		// Các thao tác trên mảng: truy xuất 1 ptu, thuộc tính và duyệt các phần tử
		for(int idx = 0; idx < arrInt.length; idx++) {
			System.out.println("element " + arrInt[idx]);
		}
		for (String element: arrStr) {
			System.out.println(element);
		}

		// Thao tác khác -> sort
		Comparator<Double> comp = new Comparator<Double>() {

			@Override
			/**
			 * ret > 0: o1 > o2
			 * ret = 0: o1 = o2;
			 * ret < 0: o1 < o2
			 */
			public int compare(Double o1, Double o2) {
				// TODO Auto-generated method stub
				return o2.compareTo(o1);
			}
			
		};
		Arrays.sort(arrDou, comp);
		for (Double element: arrDou) {
			System.out.println(element);
		}
	}

}
