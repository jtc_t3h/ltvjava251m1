package chapter6;

public class TestPolymorphism {
	
	/** 
	 * Static block chạy trước hàm main()
	 */
	static {
		System.out.println("Static block");
	}

	public static void main(String[] args) {

		// Compile time: biến hh là đối tượng của HinhHoc
		// Runtime: biến hh là đối tượng của HinhHoc
//		HinhHoc hh = new HinhHoc();
		HinhHoc hh = null;
//		System.out.println(hh.tinhCV());

		// Compile time: biến hh là đối tượng của HinhHoc
		// Runtime: biến hh là đối tượng của HinhTron
		hh = new HinhTron(2);
		System.out.println(hh.tinhCV()); // hàm tinhCV là của HinhTron
		
		// Compile time: biến hh là đối tượng của HinhHoc
		hh = new HinhChuNhat(2, 3);
		System.out.println(hh.tinhCV()); // Hàm tinhCV là của HinhfChuNhat
		
	}

}
