package chapter6;

// Thua ke an den Object
public abstract class HinhHoc {

	// overload
	public HinhHoc() {
		System.out.println("HinhHoc contructor with no-args.");
	}
	
	public HinhHoc(String name){
		System.out.println("Hinhhoc contructor with 1-arg.");
	}
	
	public HinhHoc(int id){
		System.out.println("Hinhhoc contructor with 1-arg.");
	}
	
	protected abstract double tinhCV();
}
