package chapter6;

public abstract class Sach {

	private String maSach;
	private String tenSach;
	private String ngayNhap;
	private String nxb;
	private Long donGia;
	private Integer soLuong;
	
	private static double tongTien;

	public String getMaSach() {
		return maSach;
	}

	public void setMaSach(String maSach) {
		this.maSach = maSach;
	}

	public String getTenSach() {
		return tenSach;
	}

	public void setTenSach(String tenSach) {
		this.tenSach = tenSach;
	}

	public String getNgayNhap() {
		return ngayNhap;
	}

	public void setNgayNhap(String ngayNhap) {
		this.ngayNhap = ngayNhap;
	}

	public String getNxb() {
		return nxb;
	}

	public void setNxb(String nxb) {
		this.nxb = nxb;
	}

	public Long getDonGia() {
		return donGia;
	}

	public void setDonGia(Long donGia) {
		this.donGia = donGia;
	}

	public Integer getSoLuong() {
		return soLuong;
	}

	public void setSoLuong(Integer soLuong) {
		this.soLuong = soLuong;
	}

	public abstract double thanhTien();
	
	public static double getTongTien(Sach sach) {
		tongTien = tongTien + sach.thanhTien();
		return tongTien;
	}

	@Override
	public String toString() {
		return maSach + " - " + tenSach + " - " + ngayNhap + " - " + nxb + " - " + donGia + " - " + soLuong;
	}
	
	
}
