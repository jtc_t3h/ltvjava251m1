package chapter6;

public abstract class NhanVien {

	protected final long luongCB = 1550000;
	private double heSo;
	
	public NhanVien() {
	}
	
	public NhanVien(double heSo){
		this.heSo = heSo;
	}
	
	public double getHeSo() {
		return heSo;
	}

	public void setHeSo(double heSo) {
		this.heSo = heSo;
	}

	public abstract double tinhLuong();
}
