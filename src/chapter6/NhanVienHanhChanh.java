package chapter6;

public class NhanVienHanhChanh extends NhanVien{

	private long phuCap;
	
	public NhanVienHanhChanh() {
	}
	
	public NhanVienHanhChanh(double heSo, long phuCap) {
		super(heSo);
		this.phuCap = phuCap;
	}

	@Override
	public double tinhLuong() {
		return luongCB * getHeSo() + phuCap;
	}
}
