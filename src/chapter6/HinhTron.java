package chapter6;


public class HinhTron extends HinhHoc implements HinhHocInt{

	private double banKinh;
	
	public HinhTron() {
		super();
	}
	
	public HinhTron(double banKinh) {
		super("ABC");
		this.banKinh = banKinh;
	}
	
	@Override
	public double tinhCV() {
//		super.tinhCV();
		return 2 * banKinh * Math.PI;
	}

	@Override
	public double tinhDT() {
		// TODO Auto-generated method stub
		return 0;
	}
}
