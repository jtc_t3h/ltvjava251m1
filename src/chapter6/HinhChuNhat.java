package chapter6;

public class HinhChuNhat extends HinhHoc{

	private double a;
	private double b;
	
	public HinhChuNhat(double a, double b) {
		this.a = a;
		this.b = b;
	}
	
	@Override
	protected double tinhCV() {
		return (a + b) * 2;
	}
	
	
}
