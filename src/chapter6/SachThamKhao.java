package chapter6;

public class SachThamKhao extends Sach{

	private float thue;

	public float getThue() {
		return thue;
	}

	public void setThue(float thue) {
		this.thue = thue;
	}

	@Override
	public double thanhTien() {
		return getSoLuong() * getDonGia() + getSoLuong() * getDonGia() * thue/100D;
	}
	
	
}
