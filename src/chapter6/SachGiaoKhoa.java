package chapter6;

public class SachGiaoKhoa extends Sach {

	private int trangThai; // 0: select, 1: Moi, 2: Cu
	

	public int getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(int trangThai) {
		this.trangThai = trangThai;
	}

	@Override
	public double thanhTien() {
		double thanhTien = 0.0;
		if (trangThai == 1) {
			thanhTien = getSoLuong() * getDonGia();
		} else if (trangThai == 2) {
			thanhTien = getSoLuong() * getDonGia() * 0.5;
		}
		
		return thanhTien;
	}
	
	
}
