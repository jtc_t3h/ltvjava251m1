package chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmSach extends JFrame {

	private JPanel contentPane;
	private JTextField txtMaSach;
	private JTextField txtTenSach;
	private JTextField txtNgayNhap;
	private JTextField txtNXB;
	private JTextField txtDonGia;
	private JTextField txtSoLuong;
	private JTextField txtThue;
	
	private ButtonGroup bgSach = new ButtonGroup();
	private JTextField txtTongTienSGK;
	private JTextField txtTongTienSTK;
	private JTextField txtDonGiaTB;
	private JLabel lblThue;
	private JLabel lblTinhTrang;
	private JComboBox cbbTinhTrang;
	private JTabbedPane tabbedPane;
	private JPanel pnSGK;
	private JList listSGK;
	private JList listSTK;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmSach frame = new FrmSach();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmSach() {
		setTitle("Quản lý sách");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 684, 633);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Th\u00F4ng tin chung", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(6, 6, 672, 221);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Mã sách");
		lblNewLabel.setBounds(16, 28, 76, 16);
		panel.add(lblNewLabel);
		
		txtMaSach = new JTextField();
		txtMaSach.setBounds(104, 23, 157, 26);
		panel.add(txtMaSach);
		txtMaSach.setColumns(10);
		
		JLabel lblTnSch = new JLabel("Tên sách");
		lblTnSch.setBounds(295, 28, 101, 16);
		panel.add(lblTnSch);
		
		txtTenSach = new JTextField();
		txtTenSach.setColumns(10);
		txtTenSach.setBounds(408, 23, 247, 26);
		panel.add(txtTenSach);
		
		JLabel lblNgyNhp = new JLabel("Ngày nhập");
		lblNgyNhp.setBounds(16, 66, 76, 16);
		panel.add(lblNgyNhp);
		
		txtNgayNhap = new JTextField();
		txtNgayNhap.setColumns(10);
		txtNgayNhap.setBounds(104, 61, 157, 26);
		panel.add(txtNgayNhap);
		
		JLabel lblNxb = new JLabel("NXB");
		lblNxb.setBounds(295, 66, 101, 16);
		panel.add(lblNxb);
		
		txtNXB = new JTextField();
		txtNXB.setColumns(10);
		txtNXB.setBounds(408, 61, 247, 26);
		panel.add(txtNXB);
		
		JLabel lblnGi = new JLabel("Đơn giá");
		lblnGi.setBounds(16, 104, 76, 16);
		panel.add(lblnGi);
		
		txtDonGia = new JTextField();
		txtDonGia.setColumns(10);
		txtDonGia.setBounds(104, 99, 157, 26);
		panel.add(txtDonGia);
		
		JLabel lblSLng = new JLabel("Số lượng");
		lblSLng.setBounds(295, 104, 101, 16);
		panel.add(lblSLng);
		
		txtSoLuong = new JTextField();
		txtSoLuong.setColumns(10);
		txtSoLuong.setBounds(408, 99, 247, 26);
		panel.add(txtSoLuong);
		
		JRadioButton rdbSGK = new JRadioButton("Sách giáo khoa");
		rdbSGK.setSelected(true);
		rdbSGK.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// Ẩn trường thuế
				lblThue.setVisible(false);
				txtThue.setVisible(false);
				
				// Hiển thi trường tình trạng
				lblTinhTrang.setVisible(true);
				cbbTinhTrang.setVisible(true);
				
				// Chọn tab hiển thị là SGK
				tabbedPane.setSelectedComponent(pnSGK);
			}
		});
		rdbSGK.setBounds(6, 138, 141, 23);
		panel.add(rdbSGK);
		bgSach.add(rdbSGK);
		
		JRadioButton rdbSTK = new JRadioButton("Sách tham khảo");
		rdbSTK.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// Hiển thị trường thuế
				lblThue.setVisible(true);
				txtThue.setVisible(true);
				
				// Ẩn trường tình trạng
				lblTinhTrang.setVisible(false);
				cbbTinhTrang.setVisible(false);
				
				// Chọn tab hiển thị là STK
				tabbedPane.setSelectedIndex(1);
			}
		});
		rdbSTK.setBounds(282, 138, 141, 23);
		panel.add(rdbSTK);
		bgSach.add(rdbSTK);
		
		lblTinhTrang = new JLabel("Tình trạng");
		lblTinhTrang.setBounds(16, 178, 76, 16);
		panel.add(lblTinhTrang);
		
		lblThue = new JLabel("Thuế (1 - 100%)");
		lblThue.setBounds(295, 178, 101, 16);
		panel.add(lblThue);
		
		txtThue = new JTextField();
		txtThue.setColumns(10);
		txtThue.setBounds(408, 173, 247, 26);
		panel.add(txtThue);
		
		cbbTinhTrang = new JComboBox();
		cbbTinhTrang.setModel(new DefaultComboBoxModel(new String[] {"Select", "Mới", "Cũ"}));
		cbbTinhTrang.setBounds(104, 174, 157, 27);
		panel.add(cbbTinhTrang);
		
		JButton btnNhpMi = new JButton("Nhập sách");
		btnNhpMi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// Tạo đối tượng Sach -> Hoặc SGK hoặc STK
				Sach sach = null;
				if (rdbSGK.isSelected()) {
					sach = new SachGiaoKhoa();
					((SachGiaoKhoa)sach).setTrangThai(cbbTinhTrang.getSelectedIndex());
				} else {
					sach = new SachThamKhao();
					((SachThamKhao)sach).setThue(Float.parseFloat(txtThue.getText().trim()));
				}
				sach.setMaSach(txtMaSach.getText().trim());
				sach.setTenSach(txtTenSach.getText().trim());
				sach.setNgayNhap(txtNgayNhap.getText().trim());
				sach.setNxb(txtNXB.getText().trim());
				sach.setDonGia(Long.parseLong(txtDonGia.getText().trim()));
				sach.setSoLuong(Integer.parseInt(txtSoLuong.getText().trim()));
				
				// Hiện thị tổng thành tiền
				txtTongTienSGK.setText(String.valueOf(Sach.getTongTien(sach)));
				
				// Hiện thị List
				DefaultListModel<Sach> model = (DefaultListModel<Sach>) listSGK.getModel();
				model.addElement(sach);
			}
		});
		btnNhpMi.setBounds(108, 246, 117, 29);
		contentPane.add(btnNhpMi);
		
		JButton btnTipTc = new JButton("Tiếp tục");
		btnTipTc.setBounds(302, 246, 117, 29);
		contentPane.add(btnTipTc);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(6, 281, 672, 313);
		contentPane.add(tabbedPane);
		
		pnSGK = new JPanel();
		tabbedPane.addTab("Sách giáo khoa", null, pnSGK, null);
		pnSGK.setLayout(null);
		
		JLabel lblTngThnhTin = new JLabel("Tổng thành tiền");
		lblTngThnhTin.setBounds(12, 28, 112, 16);
		pnSGK.add(lblTngThnhTin);
		
		txtTongTienSGK = new JTextField();
		txtTongTienSGK.setColumns(10);
		txtTongTienSGK.setBounds(136, 23, 183, 26);
		pnSGK.add(txtTongTienSGK);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 70, 639, 191);
		pnSGK.add(scrollPane);
		
		listSGK = new JList();
		scrollPane.setViewportView(listSGK);
		listSGK.setModel(new DefaultListModel<Sach>());
		
		JPanel pnSTK = new JPanel();
		tabbedPane.addTab("Sách tham khảo", null, pnSTK, null);
		pnSTK.setLayout(null);
		
		JLabel label = new JLabel("Tổng thành tiền");
		label.setBounds(19, 24, 112, 16);
		pnSTK.add(label);
		
		txtTongTienSTK = new JTextField();
		txtTongTienSTK.setColumns(10);
		txtTongTienSTK.setBounds(143, 19, 121, 26);
		pnSTK.add(txtTongTienSTK);
		
		JLabel label_1 = new JLabel("Đơn giá trung bình");
		label_1.setBounds(298, 24, 151, 16);
		pnSTK.add(label_1);
		
		txtDonGiaTB = new JTextField();
		txtDonGiaTB.setColumns(10);
		txtDonGiaTB.setBounds(461, 19, 158, 26);
		pnSTK.add(txtDonGiaTB);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(6, 50, 639, 211);
		pnSTK.add(scrollPane_1);
		
		listSTK = new JList();
		scrollPane_1.setViewportView(listSTK);
		
		// Ẩn trường thuế
		lblThue.setVisible(false);
		txtThue.setVisible(false);
	}
}
