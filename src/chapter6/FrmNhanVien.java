package chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class FrmNhanVien extends JFrame {

	private JPanel contentPane;
	private JTextField txtHeSoLuong;
	private JTextField txtThayDoi;
	private JTextField txtLuong;
	private JLabel lblThayDoi;
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmNhanVien frame = new FrmNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmNhanVien() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 247);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Loại NV");
		lblNewLabel.setBounds(6, 17, 125, 16);
		contentPane.add(lblNewLabel);
		
		comboBox = new JComboBox();
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (comboBox.getSelectedIndex() == 1) {// Hanh chinh
					// Hiện thị lblThaydoi và txtThayDoi phù hợp theo loại nhân viên
					lblThayDoi.setText("Phụ cấp");
					lblThayDoi.setVisible(true);
					txtThayDoi.setVisible(true);
				} else if (comboBox.getSelectedIndex() == 2) {
					lblThayDoi.setText("Số dự án");
					lblThayDoi.setVisible(true);
					txtThayDoi.setVisible(true);
				} else if (comboBox.getSelectedIndex() == 3) {
					lblThayDoi.setText("Số sản phẩm");
					lblThayDoi.setVisible(true);
					txtThayDoi.setVisible(true);
				} else if (comboBox.getSelectedIndex() == 4) {
					lblThayDoi.setVisible(false);
					txtThayDoi.setVisible(false);
				}
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"--- Select ---", "Hành chính", "Kỹ Thuật", "Kinh Doanh", "Lãnh Đạo"}));
		comboBox.setBounds(153, 12, 291, 27);
		contentPane.add(comboBox);
		
		JLabel lblHSLng = new JLabel("Hệ số lương");
		lblHSLng.setBounds(6, 52, 125, 16);
		contentPane.add(lblHSLng);
		
		lblThayDoi = new JLabel("New label");
		lblThayDoi.setBounds(6, 80, 125, 16);
		contentPane.add(lblThayDoi);
		// Ẩn component khi load form lần đầu
		lblThayDoi.setVisible(false);
		
		txtHeSoLuong = new JTextField();
		txtHeSoLuong.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					tinhLuong();
				}
			}
		});
		txtHeSoLuong.setBounds(152, 47, 292, 26);
		contentPane.add(txtHeSoLuong);
		txtHeSoLuong.setColumns(10);
		
		txtThayDoi = new JTextField();
		txtThayDoi.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					tinhLuong();
				}
			}
		});
		txtThayDoi.setColumns(10);
		txtThayDoi.setBounds(152, 75, 292, 26);
		contentPane.add(txtThayDoi);
		// Ẩn component khi load form lần đầu
		txtThayDoi.setVisible(false);
		
		JButton btnTinhLuong = new JButton("Tính Lương");
		btnTinhLuong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tinhLuong();
			}
		});
		btnTinhLuong.setBounds(153, 146, 117, 29);
		contentPane.add(btnTinhLuong);
		
		JLabel lblLng = new JLabel("Lương");
		lblLng.setBounds(6, 187, 125, 16);
		contentPane.add(lblLng);
		
		txtLuong = new JTextField();
		txtLuong.setColumns(10);
		txtLuong.setBounds(152, 182, 292, 26);
		contentPane.add(txtLuong);
	}
	
	public void tinhLuong() {
		double heSo = Double.parseDouble(txtHeSoLuong.getText().trim());
		NhanVien nv = null;
		if (comboBox.getSelectedIndex() == 1) {// Hanh chinh
			// B1. Lấy dữ liệu người dùng nhập -> tương ứng với từng loại nhân viên
			long phuCap = Long.parseLong(txtThayDoi.getText().trim());
			
			// B2. Tạo ra đối tượng tương ứng với loại nhân viên
			nv = new NhanVienHanhChanh(heSo, phuCap);
		} else if (comboBox.getSelectedIndex() == 2) {
			// B1. Lấy dữ liệu người dùng nhập -> tương ứng với từng loại nhân viên
			long soDuAn = Long.parseLong(txtThayDoi.getText().trim());
			
			// B2. Tạo ra đối tượng tương ứng với loại nhân viên
//			nv = new NhanVienKyThuat(heSo, soDuAn);
		} else if (comboBox.getSelectedIndex() == 3) {
			// B1. Lấy dữ liệu người dùng nhập -> tương ứng với từng loại nhân viên
			long soDuAn = Long.parseLong(txtThayDoi.getText().trim());
			
			// B2. Tạo ra đối tượng tương ứng với loại nhân viên
//			nv = new NhanVienKyThuat(heSo, soDuAn);
		} else if (comboBox.getSelectedIndex() == 4) {
			// B2. Tạo ra đối tượng tương ứng với loại nhân viên
//			nv = new NhanVienKyThuat(heSo, soDuAn);
		}
		
		 //B3. Gọi hàm tính lươn và hiện thị kết quả
		txtLuong.setText(String.valueOf(nv.tinhLuong()));

	}
}
