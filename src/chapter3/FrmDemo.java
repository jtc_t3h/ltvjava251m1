package chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FrmDemo extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	
	private ButtonGroup genderGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmDemo frame = new FrmDemo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmDemo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 711, 402);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton button = new JButton("Open Dialog");
		button.setBounds(5, 5, 701, 29);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DlgDemo dlg = new DlgDemo();
				dlg.setModal(true);
				dlg.setLocationRelativeTo(contentPane);
				dlg.setVisible(true);
			}
		});
		contentPane.setLayout(null);
		contentPane.add(button);
		
		JPanel panel = new JPanel();
		panel.setBounds(5, 123, 701, 252);
		panel.setBorder(new TitledBorder(null, "K\u1EBFt qu\u1EA3 t\u00ECm ki\u1EBFm", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(57, 40, 61, 16);
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(130, 35, 417, 26);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnBrowser = new JButton("Browser");
		btnBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser();
				// Nếu như dialog mở ra và người dùng có chọn file thì xử lý
//				jfc.showOpenDialog(null);
				jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					File fileSelected = jfc.getSelectedFile();
					textField.setText(fileSelected.getAbsolutePath());
				}
			}
		});
		btnBrowser.setBounds(559, 35, 117, 29);
		panel.add(btnBrowser);
		
		JComboBox comboBox = new JComboBox();
//		comboBox.setModel(new DefaultComboBoxModel(new String[] {"-- Select --", "P. CNTT", "P. CSKH"}));
		
		// Cách load dữ liệu động cho combobox
		// B1: tạo ra mảng tĩnh (có thể lấy từ database)
		String[] data = {"-- Select --", "P. CNTT", "P. CSKH", "P. BHM", "P. TCHC"};
		// B2: Tạo đối tượng DefaultComboboxModel
		DefaultComboBoxModel model = new DefaultComboBoxModel(data);
		// B3: thiết lập thuộc tính model
		comboBox.setModel(model);
		
		
		comboBox.setBounds(130, 73, 411, 27);
		panel.add(comboBox);
		
		JLabel lblnV = new JLabel("Đơn vị");
		lblnV.setBounds(57, 77, 61, 16);
		panel.add(lblnV);
		
		JRadioButton rdbtnNam = new JRadioButton("Nam");
		rdbtnNam.setSelected(true);
		rdbtnNam.setBounds(15, 56, 141, 23);
		contentPane.add(rdbtnNam);
		genderGroup.add(rdbtnNam);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Nữ");
		rdbtnNewRadioButton.setBounds(103, 56, 72, 23);
		contentPane.add(rdbtnNewRadioButton);
		genderGroup.add(rdbtnNewRadioButton);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(366, 46, 61, 65);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblLabelOthers = new JLabel("Label Others");
		lblLabelOthers.addMouseListener(new MouseAdapter() { // Anonymous inner class -> lớp lồng cấp vô danh
			@Override
			public void mouseClicked(MouseEvent e) {
				JOptionPane.showMessageDialog(null, "Hello, how are you?");
			}
		});
		lblLabelOthers.setBounds(557, 60, 90, 29);
		contentPane.add(lblLabelOthers);
		lblNewLabel_1.addMouseListener(new MyMouseListener());
	}
}
