package chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class TabPanelDemo extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TabPanelDemo frame = new TabPanelDemo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TabPanelDemo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(5, 5, 440, 267);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Hình Tròn", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblBnKinh = new JLabel("Bán Kinh");
		lblBnKinh.setBounds(18, 20, 61, 16);
		panel.add(lblBnKinh);
		
		textField = new JTextField();
		textField.setBounds(114, 15, 299, 26);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnTnh = new JButton("Tính");
		btnTnh.setBounds(155, 164, 117, 29);
		panel.add(btnTnh);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Hình Chữ Nhât", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblChiuDi = new JLabel("Chiều dài");
		lblChiuDi.setBounds(6, 18, 61, 16);
		panel_1.add(lblChiuDi);
		
		JLabel lblChiuRng = new JLabel("Chiều rộng");
		lblChiuRng.setBounds(6, 46, 96, 16);
		panel_1.add(lblChiuRng);
		
		textField_1 = new JTextField();
		textField_1.setBounds(113, 13, 300, 26);
		panel_1.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(114, 41, 300, 26);
		panel_1.add(textField_2);
	}
}
